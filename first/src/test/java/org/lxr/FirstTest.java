package org.lxr;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FirstTest {

    @Test
    @DisplayName("First Test")
    void first() {
        assertEquals(5, 5);
    }

    @Test
    void addOne() {
        assertEquals(5, Main.addOne(4));
    }

}
